package de.ur.winfo4.krypto.Kasiski;

import de.ur.winfo4.krypto.Util.Util;

import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * Die Kasiski-Test Klasse. Sie geht davon aus, dass die Primfaktor-Fragmente die sich oft überschneiden, das beste Ergebnis des Kasiski-Tests sind.
 *
 */
public class Kasiski {

    private String _cipher;
    private int _min_length;
    private int _max_length;
    private int _step;
    private ArrayList<String> _known_pattern;
    private ArrayList<Tuple_Count> _tuple_count;


    /**
     * Konstruktor der Kasiski-Klasse
     *
     * @param cipher Verschlüsselter Text der untersucht werden soll.
     * @param step Schrittgröße zwischen der minimalen und maximalen N-Gram länge.
     * @param min_length Minimale N-Gram länge.
     * @param max_length Maximale n-Gram länge.
     */

    public Kasiski(String cipher, int step, int min_length, int max_length) {
        this._cipher = cipher.replaceAll(" ", "").replaceAll("\n", "").replaceAll("\r", "").toUpperCase();
        this._step = step;
        this._min_length = min_length;
        this._max_length = max_length;
        this._known_pattern = new ArrayList<>();
        this._tuple_count = test();

        this._tuple_count.sort(new Comparator<Tuple_Count>() {
            @Override
            public int compare(Tuple_Count o1, Tuple_Count o2) {
                return o2.getWeight() - o1.getWeight();
            }
        });
    }

    /**
     * Gibt eine ArrayList mit den besten Schlüssellängen Kandidaten zurück.
     * @param count Maximale Anzahl der angeforderten Schlüsselkandidaten.
     * @return ArrayListe der besten Schlüssellängenkanidaten.
     */

    public ArrayList<Integer> get_best_multiplied_key_lengths(int count){
        ArrayList<Integer> best_lengts = new ArrayList<>();

        for(int i = 0; (i < count) && (i < _tuple_count.size()); i++ ){
            best_lengts.add(Util.multiply_array(_tuple_count.get(i).get_prime_tuple()));
        }

        return best_lengts;
    }

    /**
     * Anstoßmethode des Kassiskitests
     * @return
     */
    private ArrayList<Tuple_Count> test() {
        return test(_cipher, _min_length, _max_length);
    }

    /**
     * Gibt das Ergebnis des Kasiski-Tests als eine ArrayList von Tuple_Counts zurück. Die besten Ergebnisse haben dabei den kleinsten Index.
     * @param cipher Verschlüsselter Text der untersucht wird.
     * @param min_length Minimale N-Gram länge.
     * @param max_length Maximale N-Gram länge.
     * @return Ergebnis des Kasiski-Tests
     */
    private ArrayList<Tuple_Count> test(String cipher, int min_length, int max_length) {
        ArrayList<Tuple_Count> tmp = new ArrayList<>();
        ArrayList<Length_Distance_Pair> ldp_list = get_length_distance_pairs(_cipher, _min_length, _max_length);

        // Diese Schleife macht nichts anderes als jedes Pattern zu untersuchen und die Primfaktorzerlegung zu testen!
        for (Length_Distance_Pair ldp : ldp_list) {
            for (Pattern_Distance_Pair pdp : ldp.getPattern_distance_pairs()) {
                for (Distance dist : pdp.get_distances()) {
                    ArrayList<ArrayList<Integer>> powerList = Util.powerList(dist.get_primefactors());

                    //System.out.println(dist.get_primefactors().size() + ":" + powerList.size() + " - " + powerList);
                    for (ArrayList<Integer> power_list_sub_set : Util.powerList3(dist.get_primefactors())) {
                        if (power_list_sub_set.size() > 0) {
                            Tuple_Count tc = new Tuple_Count(power_list_sub_set);
                            if (tmp.contains(tc)) {
                                int index = tmp.indexOf(tc);
                                Tuple_Count tmp_tc = tmp.get(index);
                                tmp_tc.setCount(tmp_tc.getCount() + 1);
                            } else {
                                tmp.add(tc);
                            }
                        }
                    }
                }
            }
        }


        return tmp;
    }

    /**
     * Generiert eine ArrayList aus Length_Distance_Pairs. Dabei wird ein Pair erstellt pro N-Gram Lännge.
     * @param cipher Der Verschlüsstelte Text der untersucht werden soll.
     * @param min_length Minimale N-Gram länge.
     * @param max_length Maximale N-Gram länge.
     *
     * @return ArrayList aus Length_Distance_Pairs
     */
    private ArrayList<Length_Distance_Pair> get_length_distance_pairs(String cipher, int min_length, int max_length) {
        ArrayList<Length_Distance_Pair> length_distance_pairs = new ArrayList<>();

        for (int i = min_length; i <= max_length; i += _step) {
            Length_Distance_Pair ldp = search_distance_for_length(cipher, i);
            length_distance_pairs.add(ldp);
        }

        return length_distance_pairs;
    }

    /**
     * Erstellt ein Length_Distance_Pair.
     * @param cipher Verschlüsselter Text
     * @param length N-Gramm länge
     * @return Length_Distance_Pair für alle Patterns der Länge "length"
     */
    private Length_Distance_Pair search_distance_for_length(String cipher, int length) {
        Length_Distance_Pair pair = new Length_Distance_Pair(length);

        for (int i = 0; i < (cipher.length() - length); i++) {
            String pattern = cipher.substring(i, i + length);


            if (!_known_pattern.contains(pattern)) {
                _known_pattern.add(pattern);
                ArrayList<Integer> pat_indexes = find_indexes(pattern);
                if (pat_indexes.size() > 1) {
                    ArrayList<Distance> pat_distances = distances(pat_indexes);
                    Pattern_Distance_Pair pdp = new Pattern_Distance_Pair(pattern, pat_indexes, pat_distances);
                    pair.getPattern_distance_pairs().add(pdp);
                }
            }
        }
        return pair;
    }

    /**
     * Findet alle Indexes des übergebenen Patterns
     * @param pattern Pattern nachdem gesucht werden soll.
     * @return ArrayList mit allen Indexen als Integer
     */

    public ArrayList<Integer> find_indexes(String pattern) {
        ArrayList<Integer> indexes = new ArrayList<>();

        if (_cipher.contains(pattern)) {
            indexes.add(_cipher.indexOf(pattern));
            int last_index = _cipher.lastIndexOf(pattern);

            while (indexes.get(indexes.size() - 1) != last_index) {
                int last_found_index = indexes.get(indexes.size() - 1);
                int next_found_index = _cipher.indexOf(pattern, last_found_index + 1);

                indexes.add(next_found_index);
            }
        }
        return indexes;
    }

    /**
     * Berechnet die Distanz zwischen allen aufkommenden Indexen
     * @param indexes ArrayList mit Indexen der Wortvorkomnisse
     * @return ArrayList mit allen Distanzen zwischen den Indexen
     */
    public ArrayList<Distance> distances(ArrayList<Integer> indexes) {
        ArrayList<Distance> distances = new ArrayList<>();

        if (indexes.size() >= 2) {
            for (int i = 0; i < indexes.size(); i++) {
                for (int k = i + 1; k < indexes.size(); k++) {
                    int firstIndex = indexes.get(i);
                    int secondIndex = indexes.get(k);
                    int diff = secondIndex - firstIndex;

                    Distance tmpDist = new Distance(diff);
                    if (!distances.contains(tmpDist)) {
                        distances.add(tmpDist);
                    }
                }
            }
        }


        distances.sort(new Comparator<Distance>() {
            @Override
            public int compare(Distance o1, Distance o2) {
                return o2.get_distance() - o1.get_distance();


            }
        });


        return distances;
    }


    // GETTER UND SETTER

    public String get_cipher() {
        return _cipher;
    }

    public void set_cipher(String _cipher) {
        this._cipher = _cipher;
    }

    public int get_min_length() {
        return _min_length;
    }

    public void set_min_length(int _min_length) {
        this._min_length = _min_length;
    }

    public int get_max_length() {
        return _max_length;
    }

    public void set_max_length(int _max_length) {
        this._max_length = _max_length;
    }

    public int get_step() {
        return _step;
    }

    public void set_step(int _step) {
        this._step = _step;
    }

    public ArrayList<String> get_known_pattern() {
        return _known_pattern;
    }

    public void set_known_pattern(ArrayList<String> _known_pattern) {
        this._known_pattern = _known_pattern;
    }

    public ArrayList<Tuple_Count> get_tuple_count() {
        return _tuple_count;
    }

    public void set_tuple_count(ArrayList<Tuple_Count> _tuple_count) {
        this._tuple_count = _tuple_count;
    }




}

