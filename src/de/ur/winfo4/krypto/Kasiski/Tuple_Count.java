package de.ur.winfo4.krypto.Kasiski;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Tuple_Count Strukturklasse, die die Primfaktoren beinhaltet und ihre Vorkomnisse zählt.
 */
public class Tuple_Count {
    private ArrayList<Integer> _prime_tuple;
    private int count;

    public Tuple_Count(ArrayList<Integer> prime_tuple) {
        this._prime_tuple = prime_tuple;
        count = 1;

        prime_tuple.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tuple_Count that = (Tuple_Count) o;

        if (_prime_tuple != null ? !_prime_tuple.equals(that._prime_tuple) : that._prime_tuple != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return _prime_tuple != null ? _prime_tuple.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Tuple_Count{" +
                "_prime_tuple=" + _prime_tuple +
                ", count=" + count +
                ", weight=" + getWeight() +
                '}';
    }

    public ArrayList<Integer> get_prime_tuple() {
        return _prime_tuple;
    }

    public void set_prime_tuple(ArrayList<Integer> _prime_tuple) {
        this._prime_tuple = _prime_tuple;
    }

    public int getCount() {
        return count;
    }

    public int getWeight(){
        //TODO PROOF WEIGHT * SIZE
        return count * get_prime_tuple().size();
    }

    public void setCount(int count) {
        this.count = count;
    }
}