package de.ur.winfo4.krypto.Kasiski;

import java.util.ArrayList;

/**
 * Strukturklasse die ein Pattern mit seinen Indexen und den dazugehören Distanzen beinhaltet
 *
 */

public class Pattern_Distance_Pair {
    private String _pattern;
    private ArrayList<Integer> _indexes;
    private ArrayList<Distance> _distances;

    /**
     * Konstruktor der Pattern_Distance_Pair Strukturklasse
     *
     * @param pattern Untersuchtes Pattern
     * @param indexes Indexen des Patterns
     * @param distances Distanzen zwischen des Indexen des Patterns
     */

    public Pattern_Distance_Pair(String pattern, ArrayList<Integer> indexes, ArrayList<Distance> distances) {
        this._pattern = pattern;
        this._indexes = indexes;
        this._distances = distances;
    }

    public String get_pattern() {
        return _pattern;
    }

    public void set_pattern(String _pattern) {
        this._pattern = _pattern;
    }

    public ArrayList<Distance> get_distances() {
        return _distances;
    }

    public void set_distances(ArrayList<Distance> _distances) {
        this._distances = _distances;
    }

    public ArrayList<Integer> get_indexes() {
        return _indexes;
    }

    public void set_indexes(ArrayList<Integer> _indexes) {
        this._indexes = _indexes;
    }
}