package de.ur.winfo4.krypto.Kasiski;

import de.ur.winfo4.krypto.Util.Util;

import java.util.ArrayList;

/**
  *
 * Eine Strukturklasse, die die Distanz zwischen zwei Patterns beinhaltet.
 */
public class Distance {
    private int _distance;
    private ArrayList<Integer> _primefactors;

    public Distance(int distance) {
        this._distance = distance;
        this._primefactors = Util.prime_factors(distance);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Distance distance = (Distance) o;

        if (_distance != distance._distance) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return _distance;
    }

    public int get_distance() {
        return _distance;
    }

    public void set_distance(int _distance) {
        this._distance = _distance;
    }

    public ArrayList<Integer> get_primefactors() {
        return _primefactors;
    }

    public void set_primefactors(ArrayList<Integer> _primefactors) {
        this._primefactors = _primefactors;
    }


}