package de.ur.winfo4.krypto.Kasiski;

import java.util.ArrayList;

/**
 * Eine Strukturklasse die alle Patterns einer bestimmten N-Gramm Länge beinhaltet
 */

public class Length_Distance_Pair {
    private int _length;
    ArrayList<Pattern_Distance_Pair> pattern_distance_pairs;

    /**
     * Konstruktor der Length_Distance_Pair Strukturklasse
     * @param length N-Gramm länge
     */
    public Length_Distance_Pair(int length) {
        this._length = length;
        this.pattern_distance_pairs = new ArrayList<>();
    }

    public Length_Distance_Pair(int length, ArrayList<Pattern_Distance_Pair> pattern_distance_pairs) {
        this._length = length;
        this.pattern_distance_pairs = pattern_distance_pairs;
    }

    public int get_length() {
        return _length;
    }

    public void set_length(int _length) {
        this._length = _length;
    }

    public ArrayList<Pattern_Distance_Pair> getPattern_distance_pairs() {
        return pattern_distance_pairs;
    }

    public void setPattern_distance_pairs(ArrayList<Pattern_Distance_Pair> pattern_distance_pairs) {
        this.pattern_distance_pairs = pattern_distance_pairs;
    }
}
