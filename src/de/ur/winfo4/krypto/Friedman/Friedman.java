package de.ur.winfo4.krypto.Friedman;


import de.ur.winfo4.krypto.Util.Frequency_Analyzer;

import java.util.ArrayList;

/**
 * Friedman-Test Klasse
 */
public class Friedman {

    private String _cipher;
    private Frequency_Analyzer _fa;
    private double _index_of_coincidence = -1;
    private double _key_length;

    /**
     * Friedman Konstruktor
     * @param cipher Verschlüsselter Text
     */
    public Friedman(String cipher){
        this._cipher = cipher.toUpperCase().replaceAll(" ", "").replaceAll("\n", "").replaceAll("\r", "").toUpperCase();
        this._fa = new Frequency_Analyzer(_cipher);
        _index_of_coincidence = this._fa.calculate_index_of_coincidence();
        _key_length = calculate_key_length();
    }

    /**
     * Berechnet die Key-Länge
     * @return Friedman-Test Kelängen vorschlag
     */
    public double calculate_key_length(){
        double key_length = 0;

        double numerator = 0.0377 * _cipher.length();
        double denominator = this.get_index_of_coincidence() * (_cipher.length() - 1 ) - 0.0385 * _cipher.length() + 0.0762;

        key_length = numerator / denominator;

        return key_length;
    }


    /**
     * Gibt 3 Mögliche Schlüssel für den Friedman-Test zurück.
     * Dabei wird abgerundet und jeweil der Key drunter und drüber dazugenommen.
     * @return Mögliche Key-Längen des Friedman tests
     */
    public ArrayList<Integer> possible_key_length_list(){
        ArrayList<Integer> possible_key_lengts = new ArrayList<>();
        for(int i = -1; i <2; i++){
            int key = (int) _key_length + i;
            if(key > 0) possible_key_lengts.add((int) _key_length + i);
        }
        return possible_key_lengts;
    }


    public String get_cipher() {
        return _cipher;
    }

    public void set_cipher(String _cipher) {
        this._cipher = _cipher;
    }

    public double get_index_of_coincidence() {

        return _index_of_coincidence;
    }

    public void set_index_of_coincidence(double _index_of_coincidence) {
        this._index_of_coincidence = _index_of_coincidence;
    }

    public double get_key_length() {
        return _key_length;
    }

    public void set_key_length(double _key_length) {
        this._key_length = _key_length;
    }
}
