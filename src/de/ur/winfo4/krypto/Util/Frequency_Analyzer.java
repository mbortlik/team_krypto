package de.ur.winfo4.krypto.Util;


import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 *
 *
 */
public class Frequency_Analyzer {


    private String _text;
    private ArrayList<Alpha_Frequency> _alpha_frequencies;
    private static HashMap<Character,Double> _letterFrequency = setup_german_frequencys();
    private static double german_best_coie = 2.05;


    /**
     * Konstruktor für die Frequenzanalyse.
     * @param text Der zu untersuchende Text.
     */
    public Frequency_Analyzer(String text){
        _alpha_frequencies = new ArrayList<>();
        this._text = text.toUpperCase().replaceAll(" ", "").replaceAll("\n", "").replaceAll("\r", "");
        for(int i = 0; i < 26; i++){
            Alpha_Frequency tmp =  new Alpha_Frequency((char) (i + 65) );
            _alpha_frequencies.add(tmp);
        }
        calculate_frequencys();
    }


    /**
     * Setup Funktion für die deutsche Buchstabenhäufigkeit
     * @return Eine Hashmap mit der Zuordnung der deutschen Buchstaben zu ihrer rel. Häufigkeit in deutschen Texten.
     */
    private static HashMap<Character,Double> setup_german_frequencys(){
        HashMap<Character,Double> letterFrequency = new HashMap<>();

        letterFrequency.put('A', 0.0651);
        letterFrequency.put('B', 0.0189);
        letterFrequency.put('C', 0.0306);
        letterFrequency.put('D', 0.0508);
        letterFrequency.put('E', 0.1740);
        letterFrequency.put('F', 0.0166);
        letterFrequency.put('G', 0.0301);
        letterFrequency.put('H', 0.0476);
        letterFrequency.put('I', 0.0755);
        letterFrequency.put('J', 0.0027);
        letterFrequency.put('K', 0.0121);
        letterFrequency.put('L', 0.0344);
        letterFrequency.put('M', 0.0253);
        letterFrequency.put('N', 0.0978);
        letterFrequency.put('O', 0.0251);
        letterFrequency.put('P', 0.0079);
        letterFrequency.put('Q', 0.0002);
        letterFrequency.put('R', 0.0700);
        letterFrequency.put('S', 0.0727);
        //letterFrequency.put('ß', 0.0031);
        letterFrequency.put('T', 0.0615);
        letterFrequency.put('U', 0.0435);
        letterFrequency.put('V', 0.0067);
        letterFrequency.put('W', 0.0189);
        letterFrequency.put('X', 0.0003);
        letterFrequency.put('Y', 0.0004);
        letterFrequency.put('Z', 0.0113);

        return letterFrequency;
    }

    /**
     * http://en.wikipedia.org/wiki/Index_of_coincidence
     * Berechnet die |Differenz| ggü. dem besten ioce für deutsche Texte.
     * @return Differenz ggü. dem besten ioce für deutsche Texte.
     */
    public double calculate_ioce_diff(){
        double coie = calculate_index_of_coincidence_expected();

        double diff = german_best_coie - coie;
        if(diff < 0) diff *= -1;

        return diff;
    }

    /**
     * http://en.wikipedia.org/wiki/Index_of_coincidence
     * Berechnet die relative Buchstabenhäufigkeit aufgrund der CHI-Formel.
     * @return Die relative CHI Buchstabenhäufigkeit.
     */
    public double calculate_chi(){
        double chi = 0;
        for(Alpha_Frequency af : _alpha_frequencies){
            chi += af.get_count() * _letterFrequency.get(af.get_alpha());
        }
        return chi;
    }

    /**
     * Zählt die Buchstaben im gesamten Text.
     */
    public void calculate_frequencys(){

        for(char c : _text.toCharArray()){
            Alpha_Frequency tmp_af = new Alpha_Frequency(c);
            int index = _alpha_frequencies.indexOf(tmp_af);

            Alpha_Frequency af = _alpha_frequencies.get(index);
            af.set_count(af.get_count() + 1);
        }

    }

    /**
     * http://en.wikipedia.org/wiki/Index_of_coincidence
     * Berechnet den ioce aufgrund der zuvor gezählten Buchstaben.
     * @return IoCe
     */
    public double calculate_index_of_coincidence_expected(){
        double ioce = 0;
        double numerator = 0;

        double denominator = 1d / 26d;

        for(Alpha_Frequency af : _alpha_frequencies ){
            numerator += Math.pow((double) af.get_count() / (double) _text.length(), 2);
        }
        ioce = numerator / denominator;

        return ioce;
    }

    /**
     * http://en.wikipedia.org/wiki/Index_of_coincidence
     * Berechnet den IoC
     * @return IoC
     */
    public double calculate_index_of_coincidence(){
        double ioc = 0;
        double numerator = 0;
        double denominator = _text.length() * (_text.length() - 1);

        for(Alpha_Frequency af : _alpha_frequencies ){
            numerator += af.get_count() * (af.get_count() -1 );
        }

        ioc = numerator / denominator;

        return ioc;
    }


    // Getter und Setter

    public String get_text() {
        return _text;
    }

    public void set_text(String _text) {
        this._text = _text;
    }

    public ArrayList<Alpha_Frequency> get_alpha_frequencies() {
        return _alpha_frequencies;
    }

    public void set_alpha_frequencies(ArrayList<Alpha_Frequency> _alpha_frequencies) {
        this._alpha_frequencies = _alpha_frequencies;
    }
}
