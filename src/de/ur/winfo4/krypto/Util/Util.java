package de.ur.winfo4.krypto.Util;

import java.util.*;


/**
 *
 * Utility Klasse des Projekts
 *
 * Created by micha_000 on 18.11.2014.
 */
public abstract class Util {

    /**
     * Berechnet die Produktsumme einer ArrayList aus Integern.
     * @param arr Integer Arraylist aus der die Produktsumme berechnet wird.
     * @return Produktsumme
     */
    public static int multiply_array(ArrayList<Integer> arr){
        int res = 1;

        for(int number: arr){
            res *= number;
        }

        return res;
    }


    /**
     * Berechent alle Primfaktoren einer Zahl.
     * @param number Die Zahl für die Primfaktorzerlegung
     * @return Eine ArrayListe aus Primfaktoren
     */
    public static ArrayList<Integer> prime_factors(int number){
        ArrayList<Integer> factors = new ArrayList<>();
        for (int i = 2; i <= number; i++ ) {
            if (number % i== 0) {
                factors.add(i);
                number = number/i;
                i = 1;
            }
        }

        return factors;
    }


    /**
     * Erstellt die Potenzmenge einer ArrayList wobei die leere Menge dabei außer acht gelassen wird.
     * @param originalList Die Liste aus der die Potenzmenge erstellt werden soll.
     * @return Eine zweifach verschachtelte Liste aus Integern, die die Potenzmenge der übergebenen Liste darstellt.
     */
    public static ArrayList<ArrayList<Integer>> powerList(ArrayList<Integer> originalList) {
        ArrayList<ArrayList<Integer>> lists = new ArrayList<ArrayList<Integer>>();
        HashSet<Integer> tmp_set = new HashSet<Integer>(originalList);
        Set<Set<Integer>> power_set =  powerSet(tmp_set);

        for(Set s: power_set){
            ArrayList<Integer> tmp_list = new ArrayList<>(s);
            if(tmp_list.size() > 0 ) lists.add(tmp_list);
            tmp_list.sort(new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    return o1-o2;
                }
            });
        }

        lists.sort(new Comparator<ArrayList<Integer>>() {
            @Override
            public int compare(ArrayList<Integer> o1, ArrayList<Integer> o2) {
                if(o1.size() < o2.size()){
                    return -1;
                } else if (o1.size() > o2.size()){
                    return 1;
                } else {
                    return Util.multiply_array(o1) - Util.multiply_array(o2);
                }
            }
        });
        return lists;
    }

    /**
     * Erstellt die Potenzmenge einer ArrayList wobei die leere Menge dabei außer acht gelassen wird. Gibt eine Sortierte Liste zurück.
     * @param set Die Liste aus der die Potenzmenge erstellt werden soll.
     * @return Eine zweifach verschachtelte Liste aus Integern, die die Potenzmenge der übergebenen Liste darstellt.
     */
    public static ArrayList<ArrayList<Integer>> powerList3(ArrayList<Integer> set){
        ArrayList<ArrayList<Integer>> ret_list = new ArrayList<>();
        ArrayList<ArrayList<Integer>> set_list = powerList2(set);

        for(int i = 0; i < set_list.size(); i++){
            ArrayList<Integer> tmp_list = set_list.get(i);
            tmp_list.sort(new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    return o1-o2;
                }
            });
            if(!ret_list.contains(tmp_list) && tmp_list.size() > 0){
                ret_list.add(tmp_list);
            }
        }

        ret_list.sort(new Comparator<ArrayList<Integer>>() {
            @Override
            public int compare(ArrayList<Integer> o1, ArrayList<Integer> o2) {
                if(o1.size() < o2.size()){
                    return -1;
                } else if (o1.size() > o2.size()){
                    return 1;
                } else {
                    return Util.multiply_array(o1) - Util.multiply_array(o2);
                }
            }
        });

        return ret_list;
    }

    /**
     * Erstellt die Potenzmenge einer ArrayList wobei die leere Menge dabei außer acht gelassen wird. Erstellt eine unsortierte ArrayList
     * @param originalList Die Liste aus der die Potenzmenge erstellt werden soll.
     * @return Eine zweifach verschachtelte Liste aus Integern, die die Potenzmenge der übergebenen Liste darstellt.
     */
    public static <T> ArrayList<ArrayList<T>> powerList2(ArrayList<T> originalList ){
        ArrayList<ArrayList<T>> sets = new ArrayList<ArrayList<T>>();
        if (originalList.isEmpty()) {
            sets.add(new ArrayList<T>());
            return sets;
        }
        ArrayList<T> list = new ArrayList<T>(originalList);
        T head = list.get(0);
        ArrayList<T> rest = new ArrayList<T>(list.subList(1, list.size()));
        for (ArrayList<T> set : powerList2(rest)) {
            ArrayList<T> newSet = new ArrayList<T>();
            newSet.add(head);
            newSet.addAll(set);
            sets.add(newSet);
            sets.add(set);
        }
        return sets;
    }
    /**
     * Erstellt die Potenzmenge eines Sets wobei die leere Menge dabei außer acht gelassen wird.
     * @param originalSet Ein Set aus der die Potenzmenge erstellt werden soll.
     * @return Ein zweifach verschachteltes Set aus Integern, die die Potenzmenge des übergebenen Sets darstellt.
     */
    public static <T> Set<Set<T>> powerSet(Set<T> originalSet) {
        Set<Set<T>> sets = new HashSet<Set<T>>();
        if (originalSet.isEmpty()) {
            sets.add(new HashSet<T>());
            return sets;
        }
        List<T> list = new ArrayList<T>(originalSet);
        T head = list.get(0);
        Set<T> rest = new HashSet<T>(list.subList(1, list.size()));
        for (Set<T> set : powerSet(rest)) {
            Set<T> newSet = new HashSet<T>();
            newSet.add(head);
            newSet.addAll(set);
            sets.add(newSet);
            sets.add(set);
        }
        return sets;
    }
}

