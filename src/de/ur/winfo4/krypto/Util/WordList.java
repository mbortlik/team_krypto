package de.ur.winfo4.krypto.Util;

import jdk.nashorn.internal.runtime.regexp.joni.Regex;

import java.io.*;
import java.util.ArrayList;

/**
 * WordList Hilfsklasse
 */
public class WordList {

    public static final ArrayList<String> word_list = load_wordlist();

    /**
     * Lädt eine deutsche Wortliste aus der wordlist.txt
     * @return
     */
    public static ArrayList<String> load_wordlist(){
        File list = new File("wordlist.txt");

        ArrayList<String> word_list = new ArrayList<>();
        try (
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(list)))
        ) {
            String line = "";
            while ( (line = br.readLine()) != null){
                if(line.length() > 4) word_list.add(line.toUpperCase());
            }

        } catch(Exception e){
            e.printStackTrace();
        }

        return word_list;

    }

    /**
     * Ermittelt alle möglichen Wort vorkomnisse im Übergebenen Text.
     * @param text Text der untersucht werden soll.
     * @return Die Anzahl an gefunden Wörtern.
     */
    public static int matches (String text){
        int matches = 0;


        for(String word: word_list){
            if(text.contains(word)){

                int sub_matches = (text.length() - text.replace(word, "").length())/word.length();
                matches += sub_matches;
                //System.out.println("WORD: " + word + " MATCHNR: " + sub_matches );
            }

        }

        return matches;
    }




}
