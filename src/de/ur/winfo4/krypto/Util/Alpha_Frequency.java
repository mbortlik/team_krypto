package de.ur.winfo4.krypto.Util;

/**
 * Alpha_Frequency Strukturklasse, die die einzelnen Buchstaben hochzählt.
 */
public class Alpha_Frequency {
    char _alpha;
    int _count;

    /**
     * @param alpha Character der gezählt werden soll.
     */
    public Alpha_Frequency(char alpha) {
        this._alpha = alpha;
        this._count = 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Alpha_Frequency that = (Alpha_Frequency) o;

        if (_alpha != that._alpha) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) _alpha;
        return result;
    }

    public char get_alpha() {
        return _alpha;
    }

    public void set_alpha(char _alpha) {
        this._alpha = _alpha;
    }

    public int get_count() {
        return _count;
    }

    public void set_count(int _count) {
        this._count = _count;
    }
}
