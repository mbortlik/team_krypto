package de.ur.winfo4.krypto.Vigenere;

/**
 * VigenereChiffre Klasse
 */
public abstract class VigenereChiffre {


    /**
     * Verschlüsselt den Übergebenen Text mit dem Übergebenen Schlüssel nach Vigenere
     * @param plaintext Text der verschlüsselt werden soll.
     * @param key Schlüssel
     * @return Verschlüsstelter Text
     */
    public static String encrypt(String plaintext, String key){

        plaintext = plaintext.replaceAll(" ", "").replaceAll("\n", "").replaceAll("\r", "").toUpperCase();
        StringBuilder sb = new StringBuilder();


        for (int i = 0; i < plaintext.length(); i++) {

            char plain_char = plaintext.charAt(i);
            char key_char = key.toUpperCase().charAt((i)% key.length());

            byte plain_byte = (byte) (plain_char - 65);// A = 0 ; B = 1
            byte key_byte = (byte) (key_char - 65); // A = 0; B = 1

            byte cipher_byte = (byte) ((plain_byte + key_byte) % 26); // A = 0 B = 2
            char cipher_char = (char) (cipher_byte + 65);

            sb.append(cipher_char);

        }

        return sb.toString();
    }

    /**
     * Entschlüsselt den Text übergebenen Text mit dem übergebenen Schlüssel
     * @param ciphertext Verschlüsselter Text
     * @param key Schlüssel
     * @return Entschlüsselter Text
     */
    public static String decrypt(String ciphertext, String key){
        StringBuilder sb = new StringBuilder();
        ciphertext = ciphertext.toUpperCase().replaceAll(" ", "").replaceAll("\n", "").replaceAll("\r", "");


        for (int i = 0; i < ciphertext.length(); i++) {
            char cipher_char = ' ';
            char key_char = ' ';
            try{
                cipher_char = ciphertext.toUpperCase().charAt(i);
                key_char  = key.toUpperCase().charAt((i) % key.length());
            } catch(Exception e ){
                e.printStackTrace();
            }


            byte cipher_byte = (byte) (cipher_char - 65);
            byte key_byte = (byte) (key_char - 65);

            byte plain_byte = (byte) ((cipher_byte - key_byte) % 26);
            char plain_char = (plain_byte < 0 ) ? (char) ( plain_byte + 26 + 65): (char) (plain_byte + 65);

            sb.append(plain_char);

        }

        return sb.toString();
    }

}
