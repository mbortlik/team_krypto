package de.ur.winfo4.krypto.Vigenere;

import de.ur.winfo4.krypto.Friedman.Friedman;
import de.ur.winfo4.krypto.Kasiski.Kasiski;
import de.ur.winfo4.krypto.Util.Frequency_Analyzer;
import de.ur.winfo4.krypto.Util.WordList;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Vigenere Cracker Klasse
 */
public abstract class VigenereCracker {
    /**
     * Entschlüsselt den Übergebenen Test mit Hilfe von Brute-Force, Wordlist, Kasiski Test und Friedman-Test
     * @param cipher Verschlüsselter Text
     * @return Entschlüsselter Text
     */

    public static String crack(String cipher) {
        System.out.println("START CRACKER");
        cipher = cipher.replaceAll(" ", "").replaceAll("\n", "").replaceAll("\r", "").toUpperCase();

        // Variablen SetUp
        String best_key = "";
        String plain = "";
        double best_coi_diff = Double.MAX_VALUE;
        int best_match = 0;
        int kasiski_step = 1;
        int kasiski_min_length = 3;
        int kasiski_max_length = 5;
        int kasiski_max_key_count = 5;

        // Friedman und Kasiski Test
        System.out.println("START FRIEDMAN");
        Friedman friedman = new Friedman(cipher);
        System.out.println("START KASISKI");
        Kasiski kasiski = new Kasiski(cipher, kasiski_step, kasiski_min_length, kasiski_max_length);

        ArrayList<Integer> friedman_key_candidates = friedman.possible_key_length_list();
        ArrayList<Integer> kasiski_key_candidates = kasiski.get_best_multiplied_key_lengths(kasiski_max_key_count);

        // Zusammenlegen aller möglichen Key-Längen
        ArrayList<Integer> possible_key_lengths = new ArrayList<>();
        possible_key_lengths.addAll(kasiski_key_candidates);
        possible_key_lengths.addAll(friedman_key_candidates);

        possible_key_lengths.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });
        System.out.println("POSSIBLE KEYS: " + possible_key_lengths.size());

        System.out.println("START CRACKING");


        // Brute-Force über jeden möglichen Key, Dabei entscheided die geringste IOCE Differenz über den besten KEY
        for (int possible_key_length : possible_key_lengths) {

            String tmp_key = brute_force_vigenere(cipher, possible_key_length);
            String tmp_text = VigenereChiffre.decrypt(cipher, tmp_key);
            Frequency_Analyzer tmp_fa = new Frequency_Analyzer(tmp_text);
            double tmp_coi_diff = tmp_fa.calculate_ioce_diff();
            double absolute_diff = best_coi_diff - tmp_coi_diff;
            if (absolute_diff < 0) absolute_diff *= -1;

            if (best_coi_diff > tmp_coi_diff || absolute_diff < 0.3) {
                if (absolute_diff < 0.3) {
                    int new_match = WordList.matches(tmp_text);

                    if (new_match > best_match) {
                        best_coi_diff = tmp_coi_diff;
                        best_match = new_match;
                        best_key = tmp_key;
                        plain = tmp_text;
                    }

                } else {
                    best_coi_diff = tmp_coi_diff;
                    best_match = WordList.matches(tmp_text);
                    best_key = tmp_key;

                    plain = tmp_text;
                }
            }
        }

        System.out.println("KEY: " + best_key);
        System.out.println("DECRYPTED_TEXT: " + plain);
        return plain;
    }

    /**
     * BruteForce Vigenere. der beste CHI-Wert entscheidet über den richtigen Key
     * @param cipher Verschlüsselter Text
     * @param key_length Schlüssellänge
     * @return Entschlüsselter Text
     */
    public static String brute_force_vigenere(String cipher, int key_length) {
        cipher = cipher.replaceAll(" ", "").replaceAll("\n", "").replaceAll("\r", "").toUpperCase();
        ArrayList<String> sliced_text = text_slicer(cipher, key_length);
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < sliced_text.size(); i++) {
            String act_slice = sliced_text.get(i);
            String bestKey = "";
            double best_chi = -1;
            for (int buchstabe = 0; buchstabe < 26; buchstabe++) {
                String tmp_key = "" + ((char) (buchstabe + 65));
                String decrypted_text = VigenereChiffre.decrypt(act_slice, tmp_key);
                Frequency_Analyzer fa = new Frequency_Analyzer(decrypted_text);
                double chi = fa.calculate_chi();
                if (chi > best_chi) {
                    best_chi = chi;
                    bestKey = tmp_key;
                }
            }
            sb.append(bestKey);
        }

        return sb.toString();
    }

    /**
     * Erstellt ArrayListen für die Vigenere_brute_force Methode
     * @param text Der zu zerteilende Text.
     * @param slice_scount Anzahl der "Sclices"
     * @return Der "Geslicte" Text
     */
    public static ArrayList<String> text_slicer(String text, int slice_scount) {
        ArrayList<String> text_sclices = new ArrayList<>();

        for (int i = 0; i < slice_scount; i++) {
            ArrayList<String> slice = new ArrayList<>();
            StringBuilder sb = new StringBuilder();
            for (int k = i; k < text.length(); k += slice_scount) {
                sb.append(text.substring(k, k + 1));
            }
            text_sclices.add(sb.toString());
        }
        return text_sclices;
    }
}
